package main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.TimeZone;

public class Main {
	private static final String SUFFIX = "item";
	private static final int SIZE = 100000;
	private static final int DIVIDER = 10;
	private TimeCalculator timeCalculator;

	public static void main(String[] args) {
		new Main().launch();
	}

	private void launch() {
		timeCalculator = new TimeCalculator();
		new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 2; i++) {
					System.out.println(i + " iteration");
					calcAddAll(new ArrayContainer(), new ArrayListContainer(), new LinkedListContainer(),
							new SetContainer());
				}
			}
		}).start();

	}

	private void calcAddAll(final ElementsContainer... elementsContainer) {

		for (int i = 0; i < elementsContainer.length; i++) {

			fillCollection(elementsContainer[i]);

			calcContainsObj(elementsContainer[i]);
			calcAddInMiddle(elementsContainer[i]);
			calcRemoveFromMiddle(elementsContainer[i]);
			elementsContainer[i].clear();
		}

	}

	private void calcRemoveFromMiddle(ElementsContainer elementsContainer) {
		timeCalculator.start();
		for (int i = 0; i < SIZE; i++) {
			elementsContainer.removeFromMiddle();
		}
		System.out.println(elementsContainer.getClass().getSimpleName() + " remove from middle " + SIZE
				+ " elements for " + timeCalculator.stop());
	}

	private void calcAddInMiddle(ElementsContainer elementsContainer) {
		timeCalculator.start();
		for (int i = 0; i < SIZE; i++) {
			elementsContainer.addInMiddle(getBeanById(i));
		}
		System.out.println(elementsContainer.getClass().getSimpleName() + " add in middle " + SIZE + " elements for "
				+ timeCalculator.stop());
	}

	protected void calcContainsObj(ElementsContainer elementsContainer) {
		timeCalculator.start();

		for (int i = 0; i < SIZE; i = i + DIVIDER) {
			elementsContainer.contains(getBeanById(i));
		}
		System.out.println(elementsContainer.getClass().getSimpleName() + " check contains " + SIZE / DIVIDER
				+ " elements for " + timeCalculator.stop());
	}

	private void fillCollection(ElementsContainer elementsContainer) {
		timeCalculator.start();
		for (int i = 0; i < SIZE; i++) {
			elementsContainer.add(getBeanById(i));
		}
		System.out.println(elementsContainer.getClass().getSimpleName() + " add " + SIZE + " elements for "
				+ timeCalculator.stop());
	}

	private Bean getBeanById(int id) {
		return new Bean(SUFFIX + Integer.toString(id), id);
	}

	class TimeCalculator {
		long startTime;
		private SimpleDateFormat sdf;

		public TimeCalculator() {
			sdf = new SimpleDateFormat("mm:ss.SSS");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		}

		void start() {
			startTime = System.currentTimeMillis();
		}

		String stop() {
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			return sdf.format(new Date(elapsedTime));
		}
	}

	private class Bean {
		private String text;
		private int num;

		public Bean(String text, int num) {
			this.text = text;
			this.num = num;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + num;
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Bean other = (Bean) obj;
			if (num != other.num)
				return false;
			if (text == null) {
				if (other.text != null)
					return false;
			} else if (!text.equals(other.text))
				return false;
			return true;
		}

	}

	private interface ElementsContainer {
		void add(Bean bean);

		void addInMiddle(Bean bean);

		void removeFromMiddle();

		boolean contains(Bean bean);

		void clear();

	}

	private class ArrayListContainer implements ElementsContainer {
		ArrayList<Bean> arrayList;

		public ArrayListContainer() {
			arrayList = new ArrayList<>();
		}

		@Override
		public void add(Bean bean) {
			arrayList.add(bean);
		}

		@Override
		public boolean contains(Bean bean) {
			return arrayList.contains(bean);
		}

		@Override
		public void clear() {
			arrayList.clear();
			arrayList = null;
		}

		@Override
		public void addInMiddle(Bean bean) {
			arrayList.add(arrayList.size() / 2, bean);
		}

		@Override
		public void removeFromMiddle() {
			arrayList.remove(arrayList.size() / 2);
		}

	}

	private class LinkedListContainer implements ElementsContainer {
		LinkedList<Bean> linkedList;

		public LinkedListContainer() {
			linkedList = new LinkedList<Bean>();
		}

		@Override
		public void add(Bean bean) {
			linkedList.add(bean);
		}

		@Override
		public boolean contains(Bean bean) {
			// TODO Auto-generated method stub
			return linkedList.contains(bean);
		}

		@Override
		public void clear() {
			linkedList.clear();
			linkedList = null;
		}

		@Override
		public void addInMiddle(Bean bean) {
			linkedList.add(linkedList.size() / 2, bean);
		}

		@Override
		public void removeFromMiddle() {
			linkedList.remove(linkedList.size() / 2);
		}

	}

	private class SetContainer implements ElementsContainer {
		Set<Bean> set;

		public SetContainer() {
			set = new HashSet<Bean>();
		}

		@Override
		public void add(Bean bean) {
			set.add(bean);
		}

		@Override
		public boolean contains(Bean bean) {
			// TODO Auto-generated method stub
			return set.contains(bean);
		}

		@Override
		public void clear() {
			set.clear();
			set = null;
		}

		@Override
		public void addInMiddle(Bean bean) {
			set.add(bean);

		}

		@Override
		public void removeFromMiddle() {
			set.remove(new Bean(SUFFIX + Integer.toString(set.size() / 2), set.size() / 2));

		}
	}

	private class ArrayContainer implements ElementsContainer {
		Bean[] beanArr;
		int count;

		public ArrayContainer() {
			beanArr = new Bean[SIZE];
		}

		@Override
		public void add(Bean bean) {
			beanArr[count++] = bean;
		}

		@Override
		public boolean contains(Bean bean) {
			for (Bean currentBean : beanArr) {
				if (currentBean.equals(bean)) {
					return true;
				}
			}
			return false;
		}

		@Override
		public void clear() {
			beanArr = null;
		}

		@Override
		public void addInMiddle(Bean bean) {
			beanArr[beanArr.length / 2] = bean;
		}

		@Override
		public void removeFromMiddle() {
			beanArr[--count] = null;

		}

	}

}
